[teamspeak3_voice_udp]
title="Voice UDP Port"
desc="TeamSpeak 3"
port_forward="yes"
dst.ports="9987/udp"

[teamspeak3_query_tcp]
title="Query TCP Port"
desc="TeamSpeak 3"
port_forward="yes"
dst.ports="10011/tcp"

[teamspeak3_file_transfer_tcp]
title="File Transfer TCP Port"
desc="TeamSpeak 3"
port_forward="yes"
dst.ports="30033/tcp"
