# README #

This repository is an attempt to create an installer for TeamSpeak 3 Server on Synology DS based on the German wiki page, located at [TeamSpeak 3 Server package](http://www.synology-wiki.de/index.php/TeamSpeak_3_Server_Paket).

The repository only contains the scripts and info files needed to create the package but not the TeamSpeak 3 server binaries themselves.

Use at your own risk!