#!/bin/sh

function get_version()
{
    local version=$(grep version INFO | sed 's/version=\"\(.*\)\"/\1/')
    echo ${version}
}

function get_arch()
{
    local arch=$(grep arch INFO | sed 's/arch=\"\(.*\)\"/\1/')
    echo ${arch}
}

VERSION=$(get_version)
ARCH=$(get_arch)
ARCHIVE="teamspeak3-server_linux_amd64-${VERSION}.tar"
FILENAME="${ARCHIVE}.bz2"
URL="https://files.teamspeak-services.com/releases/server/${VERSION}/${FILENAME}"
PACKAGE="TeamSpeak-${ARCH}-${VERSION}.spk"
WORKING="working"

echo "Creating ${PACKAGE}"

if [ -e ${PACKAGE} ]; then
    rm -f ${PACKAGE}
fi

echo "Version: ${VERSION}"
echo "Arch: ${ARCH}"
echo "File: ${FILENAME}"
echo "URL: ${URL}"

if [ ! -e ${FILENAME} ]; then
    echo "Downloading TeamSpeak 3 server"
    wget -q ${URL}
fi

if [ ! -e ${FILENAME} ]; then
    echo "Unable to download TeamSpeak 3 server" > /dev/stderr
    exit 1
fi

mkdir -p ${WORKING}

cp -f INFO ${WORKING}/
cp -rf scripts ${WORKING}/
cp -rf conf ${WORKING}/
cp -f PACKAGE_ICON*.PNG ${WORKING}/

bzcat ${FILENAME} > ${WORKING}/package.tar

tar --append --file=${WORKING}/package.tar etc/TeamSpeak3.sc

pushd ${WORKING} > /dev/null

gzip package.tar
mv package.tar.gz package.tgz

tar -cf ../${PACKAGE} *

popd > /dev/null

rm -rf ${WORKING}

